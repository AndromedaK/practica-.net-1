﻿using System;


namespace Practica_Basica_1
{
    class Program
    {
        static int puntaje;
        static string nombreJugador;
        static int record = 100;
        static string nombreRecord = "cristopher";


        static void Main(string[] args)
        {
            #region Record
            /*
            Console.WriteLine("Cual es tu nombre?");
            nombreJugador = Console.ReadLine();
            Console.WriteLine("Escribe Puntaje");
            puntaje = Convert.ToInt32(Console.ReadLine());
            RevisarRecord(puntaje, nombreJugador);
            RevisarRecord(6000, "Juan");
            RevisarRecord(300, "Pedro");
            Console.ReadKey();*/
            #endregion
   
            int temperatura = -5;
            string estadoAgua;
            if (temperatura < 0) estadoAgua = "solido";
            else estadoAgua = "liquido";
            Console.WriteLine("estado: "+ estadoAgua);

            temperatura = 10;
            estadoAgua = temperatura < 0 ? "solido" : "liquido";
            Console.WriteLine("estado: "+ estadoAgua);

            temperatura = 110;
            estadoAgua = temperatura > 99 ? "gaseoso" : temperatura < 0 ? "solido": "liquido";
            Console.WriteLine("estado: "+ estadoAgua);
           

            Console.ReadKey();
            
        }

        public static void RevisarRecord(int puntaje, string nombreJugador)
        {

            if (puntaje > record)
            {
                record = puntaje;
                Console.WriteLine($" El nuevo record es: {record} \ny su nombre es {nombreJugador} ");
            }
            else
            {
                Console.WriteLine($"El record sigue siendo {record} \ny el nombre del jugador es: {nombreRecord}");
            }
        }


    }
}
