using System;


namespace Practica_Basica_3{

    class Caja{

        // Miembros variables con minuscula 
        private int altura;
        //private int ancho;
        private int volumen;
        private  int largo;

        public int SuperficieFrontal
        {
            get { return  Altura * Largo; }            
        }
        
        public Caja(int largo, int altura, int ancho)
        {
            this.altura = altura;
            this.largo  = largo;
            this.Ancho  = ancho;
        }
        /*
        public void setLargo( int largo)
        {
           this.largo = largo;
        }
        public int getLargo(){
            return this.largo;
        } */
        
        public void MuestraInfo(){
            Console.WriteLine("El largo es: {0}. La altura es: {1}. El ancho es: {2}. El volumen es: {3} . La superficie frontal es: {4}",
             largo, altura, Ancho, volumen = Ancho * altura * largo, SuperficieFrontal );
        }
        
        // Utilizar directamente la propiedad como variable, Propiedades auto implementadas
        public int Ancho { get; set; }
        // Propiedad con Mayuscula 

        /*
        public int Largo{
            get{
                return largo;
            }
            set{
                largo = value;
            }
        }*/

        // Abordaje y version reducida 
        public int Largo{
            get => largo;
            set => largo = value;
        }

        // Agregar condiciones 
        public int Altura { 
            get
            {
                return altura;
            } 
            set
            {
                if (value < 0) value = -value; //throw new Exception("El tamaño debe ser positivo");
                altura = value; 
            }

         }


    }
}