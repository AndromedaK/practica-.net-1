using System;

namespace Practica_Basica_3
{
    class Humanos
    {
            //Miembro variable
            private string primerNombre;
            private string primerApellido;
            private string grupoSanguineo = "Rh+";
            private string colorOjos;
            private int edad;

            // Constructuor por defecto

            public Humanos(){

            }
            // Constructor public parametrizado
            public Humanos(string parametroNombre, string parametroApellido, string parametroColorOjos, int parametroEdad)
            {
                this.primerNombre   = parametroNombre;
                this.primerApellido = parametroApellido;
                this.colorOjos      = parametroColorOjos;
                this.edad           = parametroEdad;
            }

            // Constructor public parametrizado
            public Humanos(string parametroNombre, string parametroApellido, int parametroEdad)
            {
                this.primerNombre   = parametroNombre;
                this.primerApellido = parametroApellido;
                this.edad           = parametroEdad;
            }
            // Miembro Metodo
            public void Presentarme(){
                if (edad!=0)
                {
                    Console.WriteLine("Hola, soy {0} y Mi grupo sanguineo es: {1}. Color de ojo: {2} - Mi edad es {3} ",
                    primerNombre,
                    grupoSanguineo,
                    colorOjos,
                    edad        
                );
                    
                }
                else{
                    Console.WriteLine("Hola, soy {0} y Mi grupo sanguineo es: {1}. Color de ojos: {2}",
                    primerNombre,
                    grupoSanguineo,
                    colorOjos);
        
                }


            }
   
    }
}
