# Encapsulamiento 

public: el acceso no está restringido.
protected: el acceso está limitado a la clase contenedora o a los tipos derivados de la clase contenedora.
internal: el acceso está limitado al ensamblado actual.
protected internal: el acceso está limitado al ensamblado actual o a los tipos derivados de la clase contenedora.
private: el acceso está limitado al tipo contenedor.
private protected: el acceso está limitado a la clase contenedora o a los tipos derivados de la clase contenedora que hay en el ensamblado actual.